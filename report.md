## Feladat kiirás

- Alkalmazz IaC eszközt, mint például OpenStack Heat, AWS CloudFromation, Terraform, stb., ameynek segítségével a megírt kód egy Wordpress-t installál+konfigurál+elindít (cloud-init script), az alább részletezett hálózattal. Az IaC kóddal két virtuális gépet (VM) kell létrehozni, amelyekből az egyiken a wordpress és a web szerver, a másikon az adatbázis szerver fut.
- A kialakítandó hálózat:
  - hozzunk létre egy saját belső/privát hálózatot+alhálózatot, a VM-ek ehhez kapcsolódjanak
  - hozzunk létre vagy használjunk egy már meglévő ssh kulcsot, amit a VM-ekhez rendelünk
- OpenStack esetén (nem OpenStack, hanem pl. AWS esetén ezzel egyenértékű hálózati konfiguráció és működés az elvárt):
   - a belső/privát hálózaton legyen beállítva DNS szerver (pl. publikus Google DNS 8.8.8.8)!
   -  hozzunk létre egy routert, ami ehhez a belső/privát hálózathoz kapcsolódik
   - ezen a routeren keresztül kapcsolódjunk a külső hálózathoz (már létező hálózaty ‘ext-net’ néven)
a web szerver VM kapjon floating IP-t (külső hálózatból elérhető IP cím)
- engedélyezzük a security group beállításoknál a web, ssh, ping kapcsolatokat kívülről a web szerverhez, de az adatbázis szerverhez csak az ssh-t.

### Beadás: 
- Feltöltés a Teams felületen:
Az elkészült szkript/kód fájlok (pl. OpenStack esetén HOT yaml fájl(ok))
Egy rövid, kb. max 5 perces képernyőfelvétel (pl. Windows 10 beépített felvételi opció segítségével) arról, ahogyan a szkript fájl végrehajtásra kerül, az virtuális infrastruktúra erőforrások létrejönnek, és a wordpress szerver oldala elérhető válik. Opcionálisan lehet hangalámondással vagy feliratozással kiegészíteni, hogy mi történik a képernyőn.
- Beszámoló: néhány kérdés a leadottakkal kapcsolatban az utolsó két óra egyikén


## Erőforrásfájlok

### ec2-bastion.tf

```yaml
resource "aws_instance" "bastion" {
  depends_on = [
    aws_instance.wordpress,
    aws_instance.mysql
  ]

  # AMI ID - Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = aws_subnet.public-subnet.id

  key_name = var.keypair
  vpc_security_group_ids = [aws_security_group.bastion-sg.id]

  tags = {
    Name = "bastion"
  }
}
```
### ec2-mysql.tf

```yaml

# Launch a Webserver Instance hosting WordPress in it.
resource "aws_instance" "mysql" {
  depends_on = [
    aws_instance.wordpress,
    aws_nat_gateway.nat-gateway,
    aws_route_table_association.rt-association-ng
  ]

  # AMI ID - Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = aws_subnet.private-subnet.id
  key_name      = var.keypair
  user_data     = file("mysql.sh") 
  vpc_security_group_ids = [aws_security_group.mysql-sg.id]

  tags = {
    Name = "mysql"
  }
}
```
### ec2-wp.tf

```yaml

# Launch a Webserver Instance hosting WordPress in it.
resource "aws_instance" "wordpress" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet,
    aws_security_group.bastion-sg
  ]

  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = aws_subnet.public-subnet.id

  key_name = "tf-deploy"

  # Security groups to use
  vpc_security_group_ids = [aws_security_group.wp-sg.id]

  tags = {
    Name = "wordpress"
  }
}
```
### igw.tf

```yaml

# Create an internet gateway and attach it to the VPC.
resource "aws_internet_gateway" "igw" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet
  ]

  # VPC in which IGW has to be created
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "Internet Gateway"
  }
}
```
### keypair.tf

```yaml
resource "aws_key_pair" "keypair" {

  # Name of the Key
  key_name = var.keypair

  # Adding the SSH authorized key !
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSegx9g84by4H/zmOF8sI2d5UTCovOmJHWbzhQR6fANprBD9kqUXwyKKCBAdxdZjoX2hYX7DirU0Z7eu0jmG6xGkaC22/iPbjnVhbrOco478wnCKJV6iEl3gF9TTgJCsnE2Yv+ySiJL9YyGFanqM559ORVA0Ev9inuBvwBzEI/1xLoNhZ02oaOvqQTzR8GMzn4bAsWIifr7qoQwL0Bq7PxkUcPE9BvcjFcQ0XWk+B0WK1SyDjpIF/ECU9AXvqpDQBu12OqrhwrFiyP3KE00NA4Aai1epO1uSAlYGYvClrHP8UjCpgR810MerwoXFJl8HzVsiD9mxWbwyUreCmUpgmq4/1UPLZv5Fvmid5cSWUPYxWTSwxLc+Y+BvPlFYHF+F+0TviJHjvXFa1oLlmLAQuKSyff40kmZTnAcjQNunAkeg3Y2oK2DxHFgtYUBbhSm+pMzF3cfaIP88zjuYWKXrVG+ve1tbwuvNPzW+4bpMJoTgvEn7g+k/avsLyNuq31KVs= jani99@toolbox"
}

```
### nat-gateway-eip.tf

```yaml
resource "aws_nat_gateway" "nat-gateway" {
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [
    aws_eip.nat-gateway-eip
  ]

  # Allocating the Elastic IP to the NAT Gateway!
  allocation_id = aws_eip.nat-gateway-eip.id
  subnet_id     = aws_subnet.public-subnet.id

  tags = {
    Name = "NAT Gateway Project"
  }
}

# Creating an Elastic IP for the NAT Gateway
resource "aws_eip" "nat-gateway-eip" {
  depends_on = [
    aws_route_table_association.rt-association
  ]

  vpc = true
}

```
### nullresource-provisioners.tf

```yaml

# Create a Null Resource and Provisioners
resource "null_resource" "bastion-provisioners" {
  # Connection Block for Provisioners to connect to EC2 Instance
  connection {
    type        = "ssh"
    host        = aws_instance.bastion.public_ip
    user        = "ec2-user"
    password    = ""
    private_key = file("private-key/s50lxk")
  }

  # File Provisioner: Copies the private key file to /tmp directory
  provisioner "file" {
    source      = "private-key/s50lxk"
    destination = "/tmp/s50lxk"
  }
}
# Creation Time Provisioners - By default they are created during resource creations (terraform apply)
# Destory Time Provisioners - Will be executed during "terraform destroy" command (when = destroy)

# Create a Null Resource and Provisioners
resource "null_resource" "wp-provisioners" {
  # Connection Block for Provisioners to connect to EC2 Instance
  connection {
    type        = "ssh"
    host        = aws_instance.wordpress.public_ip
    user        = "ec2-user"
    password    = ""
    private_key = file("private-key/s50lxk")
  }

  # File Provisioner: Copies the private key file to /tmp directory
  provisioner "file" {
    source      = "private-key/s50lxk"
    destination = "/tmp/s50lxk"
  }

  # Remote Exec Provisioner: Using remote-exec provisioner fix the private key permissions on Bastion Host
  provisioner "remote-exec" {
    inline = [
      "sudo chmod 400 /tmp/s50lxk",
      "sudo yum update -y",
      "sudo yum install docker -y",
      "sudo systemctl restart docker && sudo systemctl enable docker",
      "sudo docker pull wordpress",
      "sudo docker run --name wordpress -p 80:80 -e WORDPRESS_DB_HOST=${aws_instance.mysql.private_ip} -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=root -e WORDPRESS_DB_NAME=wordpressdb -d wordpress"
    ]
  }
}
```
### route-table-ng.tf

```yaml
# Define a route table for the public subnet, specifying the internet gateway as the target for all internet-bound traffic.
resource "aws_route_table" "nat-gateway-rt" {
  depends_on = [
    aws_nat_gateway.nat-gateway
  ]

  # VPC ID
  vpc_id = aws_vpc.vpc.id

  # NAT Rule
  route {
    cidr_block = "0.0.0.0/0"

    # Identifier of a VPC NAT gateway
    nat_gateway_id = aws_nat_gateway.nat-gateway.id
  }

  tags = {
    Name = "route-table-nat-gateway"
  }
}

# Creating an Route Table Association of the NAT Gateway route table with the Private Subnet!
resource "aws_route_table_association" "rt-association-ng" {
  depends_on = [
    aws_route_table.nat-gateway-rt
  ]

  # Public Subnet ID
  subnet_id = aws_subnet.private-subnet.id

  #  Route Table ID
  route_table_id = aws_route_table.nat-gateway-rt.id
}
```
### route-table.tf

```yaml
# Define a route table for the public subnet, specifying the internet gateway as the target for all internet-bound traffic.
resource "aws_route_table" "public-subnet-rt" {
  depends_on = [
    aws_vpc.vpc,
    aws_internet_gateway.igw
  ]

  # VPC ID
  vpc_id = aws_vpc.vpc.id

  # NAT Rule
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "route-table-internet-gateway"
  }
}

# Associate the routing table to the Public Subnet to provide the Internet Gateway address.
resource "aws_route_table_association" "rt-association" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet,
    aws_route_table.public-subnet-rt
  ]

  # Public Subnet ID
  subnet_id = aws_subnet.public-subnet.id

  #  Route Table ID
  route_table_id = aws_route_table.public-subnet-rt.id
}
```
### sg-bastion.tf

```yaml

# Creating a Security Group for the Bastion Host which allows anyone in the outside world to access the Bastion Host by SSH.
resource "aws_security_group" "bastion-sg" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet
  ]

  # Name of the security group for Bastion Host
  name        = "bastion-sg"
  description = "MySQL Access only from the Webserver Instances!"

  # VPC ID in which Security group has to be created!
  vpc_id = aws_vpc.vpc.id

  # Create an inbound rule for Bastion Host SSH
  ingress {
    description = "Bastion Host SG"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outward Network Traffic from the MySQL instance
  egress {
    description = "Outbound from Bastion Host"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Bastion Host Security Group"
  }
}
```
### sg-mysql.tf

```yaml
# Create a Security Group for Mysql instance which allows database access to only those instances who are having the WordPress security group created in step 9.
resource "aws_security_group" "mysql-sg" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet,
    aws_security_group.wp-sg,
    aws_security_group.bastion-sg
  ]

  # Name of the security group for MySQL instance
  name        = "mysql-sg"
  description = "MySQL Access only from the Webserver Instances"

  # VPC ID in which Security group has to be created!
  vpc_id = aws_vpc.vpc.id

  # Create an inbound rule for MySQL
  ingress {
    description     = "MySQL Access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.wp-sg.id]
  }

  ingress {
    description     = "allow SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion-sg.id]
  }


  # Outward Network Traffic from the MySQL instance
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "MySQL Security Group"
  }
}
```
### sg-wp.tf

```yaml
# Create a Security Group for the WordPress instance, so that anyone in the outside world can access the instance by HTTP, PING, SSH
resource "aws_security_group" "wp-sg" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet,
    aws_subnet.private-subnet
  ]

  # Name of the webserver security group
  name        = "webserver-sg"
  description = "Allow outside world to access the instance via HTTP, PING, SSH"

  # VPC ID in which Security group has to be created!
  vpc_id = aws_vpc.vpc.id

  # Create an inbound rule for webserver HTTP access
  ingress {
    description = "HTTP to Webserver"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Create an inbound rule for PING
  ingress {
    description = "PING to Webserver"
    from_port   = 0
    to_port     = 0
    protocol    = "ICMP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Create an inbound rule for SSH access
  ingress {
    description = "SSH to Webserver"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outward Network Traffic from the WordPress webserver
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Wordpress Security Group"
  }
}
```
### subnet-private.tf

```yaml

# Define the public and private subnets, specifying the VPC ID, CIDR block range, and availability zone.
resource "aws_subnet" "private-subnet" {
  depends_on = [
    aws_vpc.vpc,
    aws_subnet.public-subnet
  ]

  # VPC in which subnet has to be created
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.private_subnet_range

  # AZ of this subnet
  availability_zone = var.az_private

  tags = {
    Name = "Private Subnet"
  }
}
```
### subnet-public.tf

```yaml

# Define the public and private subnets, specifying the VPC ID, CIDR block range, and availability zone.
resource "aws_subnet" "public-subnet" {
  depends_on = [
    aws_vpc.vpc
  ]

  # VPC in which subnet has to be created
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.public_subnet_range

  # AZ of this subnet
  availability_zone = var.az_public

  # Enabling automatic public IP assignment on instance launch
  map_public_ip_on_launch = true

  tags = {
    Name = "Public Subnet"
  }
}
```
### variables.tf

```yaml

# Input Vars
# Generic Vars
variable "region" {
  description = "Provides details about a specific AWS region."
  type        = string
}

variable "keypair" {
  description = "Adding the SSH authorized key"
  type        = string
}

# base_path for refrencing 
variable "base_path" {}

# VPC Vars
variable "cidr_block" {
  description = "Tags to set on the bucket"
  type        = string
}

# Public Subnet Vars
variable "public_subnet_range" {
  description = "IP Range of this subnet"
  type        = string
}

variable "az_public" {
  description = "AZ of this subnet"
  type        = string
}

# Private Subnet Vars
variable "private_subnet_range" {
  description = "IP Range of this subnet"
  type        = string
}

variable "az_private" {
  description = "AZ of this subnet"
  type        = string
}

# EC2 Wordpress Vars
variable "ami" {
  description = "AMI to use for the instance"
  type        = string
}

variable "instance_type" {
  description = "Instance type to use for the instance. "
  type        = string
}
```
### versions.tf

```yaml

# Terraform Block
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Provider Block
provider "aws" {}
```
### vpc.tf

```yaml

#  Define the VPC resource, giving it a unique name and the desired CIDR block range.
resource "aws_vpc" "vpc" {

  # IP Range for the VPC
  cidr_block = var.cidr_block

  # Enabling automatic hostname assigning
  enable_dns_support = true

  tags = {
    Name = "vpc"
  }
}
```

## Provisioning menete:

1. Erőforrás fájlok elkészítése - lásd fentebb
2. terraform plan segítsével - szemantika és szintaktikai ellenőrzés
3. terraform apply - plan végrehajtása
4. terraform destroy - létrehozott erőforrások törlése

### Elenőzzés:

**SSH:**

Wordpress VM:

```bash
$ ssh ec2-user@3.91.100.35 -i private-key/s50lxk
The authenticity of host '3.91.100.35 (3.91.100.35)' cant be established.
ED25519 key fingerprint is SHA256:kB8pglPR0cRpGG/lQ4y12tgfOTLb+kNPu6RHKE3Iwwc.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '3.91.100.35' (ED25519) to the list of known hosts.
Last login: Sun May 28 11:02:39 2023 from kszk-wifi-2.sch.bme.hu

       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-2/
[ec2-user@ip-10-0-1-254 ~]$ 
```
MySQL VM: (Bastionon keresztül)  
```bash
❯ ssh ec2-user@34.228.80.220 -i private-key/s50lxk
The authenticity of host '34.228.80.220 (34.228.80.220)' cant be established.
ED25519 key fingerprint is SHA256:/uDg41sEIANhsLreQnrUdTIo6Of5wNTcbegZkbj3zg8.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '34.228.80.220' (ED25519) to the list of known hosts.

       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-2/
29 package(s) needed for security, out of 69 available
Run "sudo yum update" to apply all updates.
[ec2-user@ip-10-0-1-218 ~]$ ssh 10.0.2.41 -i /tmp/s50lxk 
The authenticity of host '10.0.2.41 (10.0.2.41)' cant be established.
ECDSA key fingerprint is SHA256:NURMkvp8gNM3g+uRPnkp/IcmJ5dAq+aIi3tD+WS4204.
ECDSA key fingerprint is MD5:2c:07:44:c1:36:03:33:c0:ea:1e:d5:45:50:ae:af:ea.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.0.2.41' (ECDSA) to the list of known hosts.
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for '/tmp/s50lxk' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "/tmp/s50lxk": bad permissions
Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
[ec2-user@ip-10-0-1-218 ~]$ 
```

**Wordpress Web UI:**  

![WordPress Install landing page](./images/wp.png)