## This resource will create a key pair using above private key
resource "aws_key_pair" "keypair" {

  # Name of the Key
  key_name = var.keypair

  # Adding the SSH authorized key !
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSegx9g84by4H/zmOF8sI2d5UTCovOmJHWbzhQR6fANprBD9kqUXwyKKCBAdxdZjoX2hYX7DirU0Z7eu0jmG6xGkaC22/iPbjnVhbrOco478wnCKJV6iEl3gF9TTgJCsnE2Yv+ySiJL9YyGFanqM559ORVA0Ev9inuBvwBzEI/1xLoNhZ02oaOvqQTzR8GMzn4bAsWIifr7qoQwL0Bq7PxkUcPE9BvcjFcQ0XWk+B0WK1SyDjpIF/ECU9AXvqpDQBu12OqrhwrFiyP3KE00NA4Aai1epO1uSAlYGYvClrHP8UjCpgR810MerwoXFJl8HzVsiD9mxWbwyUreCmUpgmq4/1UPLZv5Fvmid5cSWUPYxWTSwxLc+Y+BvPlFYHF+F+0TviJHjvXFa1oLlmLAQuKSyff40kmZTnAcjQNunAkeg3Y2oK2DxHFgtYUBbhSm+pMzF3cfaIP88zjuYWKXrVG+ve1tbwuvNPzW+4bpMJoTgvEn7g+k/avsLyNuq31KVs= jani99@toolbox"
}
