# General
region    = "us-east-1"
keypair   = "tf-deploy"
base_path = "/var/home/jani99/aws"

# VPC
cidr_block = "10.0.0.0/16"

# Subnets
public_subnet_range  = "10.0.1.0/24"
az_public            = "us-east-1a"
private_subnet_range = "10.0.2.0/24"
az_private           = "us-east-1a"

# EC2 WordPress
ami           = "ami-0b0dcb5067f052a63"
instance_type = "t2.micro"
